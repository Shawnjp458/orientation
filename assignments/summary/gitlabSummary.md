**GitLab**

GitLab is a web-based DevOps lifecycle tool. Its repository manager is based on Git.

With GitLab you can set and modify people’s permissions according to their role.


***Features***


- wiki

- User friendly interface

- Import/Export Projects


- Issue Tracking


- Continous integration

For example, using Gitlab we can give access to Issue-Tracker without giving permission to the source code.This can  useful for teams or enterprises with role-based contributions.
