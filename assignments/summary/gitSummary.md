**Git**

Git is a distributed version-control system used for tracking changes in source code during the life cycle of a software development.

The aim of Git is to manage software development projects and its files, as they are changing over time.





***Main elements in Git***

Working Directory

Local and Remote Repository

Staging Area





***Keywords***


- Repository: A repository (also known as repo) is a collection of all the files and folders created during the process of software development.


- Commit: A commit is like taking a picture/snapshot of the files as they exist at the moment. It exists only on your local machine until it is pushed to a remote repository


- Branch: All repositories contain a main branch, i.e. the Master branch. All other tracks separate from the main branch are known as branches. These are separate instances of code that is different from main codebase.


- Merge: By using the merge command, we can integrate independant lines of development created by git branch into a single branch.

- Push: It allows us to move our work from local repo to remote repo.

- Fork: Forking allows us to create an entirely new repo of a code, instead of duplicating it, under your own name.


- Clone: Cloning takes the entire online repo and makes an exact copy of it on your local machine.